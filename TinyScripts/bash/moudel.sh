#!/bin/bash
# A shell script to login ssh or sftp.
# Written by han0x7300<han0x7300@gmail.com> 
# Last updated on, Sep-11-2017
######################################################
#                   Variables                        #
######################################################
HOST='root@192.168.3.34'
PORT='22'
# key file name or null
KEY='id_key'
KEYPATH='/home/hans/keys'

if [ -n "$1"  ] && [ -z "$KEY" ]
then
	# use sftp mode
	[ "$1" = "sftp" ] && { echo "Start sftp mod."; sftp -o port=${PORT} $HOST ; } 
elif [ -n "$1"  ] && [ -n "$KEY" ]
then
	# use sftp mode login by key,if 'KEY' is not null
	echo "start"
	[ "$1" = "sftp" ] && { echo "Start sftp mod."; sftp -oIdentityFile=$KEYPATH/$KEY -oPort=${PORT} $HOST ; } 
	
elif [ "$1" != "sftp" ] && [ -n "$KEY" ]
then
	ssh -X -i "$KEYPATH/$KEY" -p $PORT $HOST

else
	echo "Start ssh mode."
	ssh -X $HOST -p $PORT
fi

