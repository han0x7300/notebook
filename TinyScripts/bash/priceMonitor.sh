#!/bin/bash
# A shell script to auto get amazon good price.
# If pice-off price,email me.
# config OS email agent first.
# Written by han0x7300 <han0x7300@gmail.com>
# Last updated on, Aug-14-2018
######################################################
#                   Variables                        #
######################################################
#
# Good name
NAME='AMD 1700X'
#  price url
URL='https://www.amazon.com/AMD-Ryzen-1700X-Processor-YD170XBCAEWOF/dp/B06X3W9NGG/ref=sr_1_3?s=electronics&ie=UTF8&qid=1533905993'
# expect price
E_PRICE=300
# Interval time(seconds)
INTERVAL=1800
# if go_on =0,then the script exists.
go_on=1

# start run
while [ $go_on -eq 1 ]
do
		price=`curl -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0" "$URL" | grep 'data-asin-price' | awk -F\" '{ print $8 }'`
		echo "The current price is:$price"
		int=${price%.*}
		if test $int -lt $E_PRICE
		then
				 echo "yes!"
				# config MTA first,
				# see "https://gist.github.com/iandexter/467664" and
				# "https://www.linux.com/blog/setup-mutt-gmail-centos-and-ubuntu"
				echo "Current $NAME Price: $price" |  mutt -s "Price drop!!!" Username@gmail.com
				go_on=0
				exit 0
		else
			echo "$(date): $NAME still expensive($price)"
		fi
		sleep $INTERVAL
done
