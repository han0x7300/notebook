#!/bin/bash
# uncompress compressed files recursive
# sudo apt install curl wget jq yara unrar unzip p7zip-full git -y
# https://gitlab.com/han0x7300/notebook/-/tree/master/TinyScripts/bash/uncompress_all_file_recursive.sh

backupIFS=$IFS
IFS=""

# a array to store current finded compress file
# array
declare -a findCompressFileList
declare -a compressFileArray
#compressFileArray=(./1.rar)
#compressFileArray+=(./1.tar.gz)
#compressFileArray+=(./1.zip)
#compressFileArray+=(./1.7z)

#echo ${compressFileArray[@]}
#echo ${#compressFileArray[@]}

numberOfCOmpressFile=0

numberOfLoop=0

find_compress_file(){



    findCompressFileList=()

    while IFS= read -r -d '' file; do
        
        [ "$(echo $file | grep  -E -i "\.(zip|rar|tar.gz|7z|tgz|tar)$" | grep -v "/\." )" ] && { findCompressFileList+=($file); }  
        
    done < <(find . -type f -print0)
    #echo ${findCompressFileList[@]}
    #echo ${#findCompressFileList[@]}
    
    #findCompressFileList=$(find $p -type f  | grep  -E -i "\.(zip|rar|tar.gz|7z|tgz|tar)$" | grep -v "/\." )
    numberOfCOmpressFile=${#findCompressFileList[@]}
    #echo $numberOfCOmpressFile
}

find_compress_file

# array number  not equal find number
while [[ $numberOfCOmpressFile != ${#compressFileArray[@]} ]] 
do
    echo "$numberOfCOmpressFile not equal ${#compressFileArray[@]},still have some file to be operate"

    
    for file in "${findCompressFileList[@]}"
    do
        echo "$file"
        p1=$PWD
        cd "${file%/*}"
        # if compress file not in compressFileArray
        if [[ ! " ${compressFileArray[@]} " =~ " ${file} " ]]; then
            # uncompress
            #file=$(echo "\"${file}\"")
            echo "$(tput setaf 2)uncompress ${file##*/} $(tput sgr 0) in $(tput setaf 2) ${file%/*} $(tput sgr 0)"
            # decompress file
            case ${file##*/} in
                *.zip) [ "$(unzip -o ${file##*/})" ] && { echo "uncompress ${file##*/} done"; } || { echo "$(tput setaf 1)unzip ${file##*/} fail, not install unzip? $(tput sgr 0)";exit 1; } ;;

                *.rar) [ "$(unrar x -o+ ${file##*/})" ] && { echo "uncompress ${file##*/} done"; } || { echo "$(tput setaf 1)unrar ${file##*/} fail, not install unrar? $(tput sgr 0)";exit 1; } ;;

                *.7z) [ "$(7z e -y ${file##*/})" ] && { echo "uncompress ${file##*/} done"; } || { echo "$(tput setaf 1)7z ${file##*/} fail, not install p7zip-full? $(tput sgr 0)";exit 1; } ;;

                *.tar.gz) [ "$(tar -zxvf ${file##*/})" ] && { echo "uncompress ${file##*/} done"; } || { echo "$(tput setaf 1)tar.gz ${file##*/} fail, not install tar? $(tput sgr 0)";exit 1; } ;;

                *.tgz) [ "$(tar -zxvf ${file##*/})" ] && { echo "uncompress ${file##*/} done"; } || { echo "$(tput setaf 1)tgz ${file##*/} fail, not install tar? $(tput sgr 0)";exit 1; } ;;

                *.tar) [ -z "$(tar -xf ${file##*/})" ] && { echo "uncompress ${file##*/} done"; } || { echo "$(tput setaf 1)tar ${file##*/} fail, not install tar? $(tput sgr 0)";exit 1; } ;;
            esac

            # after uncompress, add to array
            compressFileArray+=(${file})
        fi
        cd $p1
    done


    echo "$(tput setaf 2)  No.$(($numberOfLoop +1)) complete!!! $(tput sgr 0)"
    echo "$(tput setaf 2)  $numberOfCOmpressFile files was uncompress. $(tput sgr 0)"
    #echo "now, array has ${#compressFileArray[@]} files:  ${compressFileArray[@]}"

    # after uncompress,check if exist new conpress file
    find_compress_file
    
    #echo "$numberOfCOmpressFile : ${#compressFileArray[@]}"
     
done

IFS=$backupIFS
