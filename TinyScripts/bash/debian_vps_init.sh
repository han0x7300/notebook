#!/bin/bash
# Debian New VPS config
# 1. install last update and upgrade to debian12
# 2. add user
# 3. config only key file login
# 4. setup iptables
# Run as root.
# wget https://gitlab.com/han0x7300/notebook/-/raw/main/TinyScripts/bash/debian_vps_init.sh -O - | bash
# bash -c "$(curl -L https://gitlab.com/han0x7300/notebook/-/raw/main/TinyScripts/bash/debian_vps_init.sh)"
# if [[ $(type -p wget) ]]; then; wget https://gitlab.com/han0x7300/notebook/-/raw/main/TinyScripts/bash/debian_vps_init.sh -O - | bash ; elif [[ $(type -p curl) ]]; then; bash -c "$(curl -L https://gitlab.com/han0x7300/notebook/-/raw/main/TinyScripts/bash/debian_vps_init.sh)" ;  fi;

# config proxy if needed.
# export https_proxy=http://172.31.xx.xx:1081
# export http_proxy=http://172.31.xx.xx:1081

USER_TO_ADD=user1
SSH_PORT=$(shuf -i 10000-65535 -n1)
DEBIAN_VERSION=bookworm  #debian 12

# If you want to run as another user, please modify $EUID to be owned by this user
check_if_running_as_root() {
  if [[ "$EUID" -ne '0' ]]; then
    echo "error: You must run this script as root!"
    exit 1
  fi
}

debian_update() {
    apt-get  update -y
    apt-get install apt-transport-https ca-certificates -y
    
    # modify source file
    cat > /etc/apt/sources.list << APT
deb https://deb.debian.org/debian $DEBIAN_VERSION main contrib non-free non-free-firmware
deb-src https://deb.debian.org/debian $DEBIAN_VERSION main contrib non-free non-free-firmware

deb https://deb.debian.org/debian-security/ $DEBIAN_VERSION-security main contrib non-free non-free-firmware
deb-src https://deb.debian.org/debian-security/ $DEBIAN_VERSION-security main contrib non-free non-free-firmware

deb https://deb.debian.org/debian $DEBIAN_VERSION-updates main contrib non-free non-free-firmware
deb-src https://deb.debian.org/debian $DEBIAN_VERSION-updates main contrib non-free non-free-firmware

deb https://deb.debian.org/debian $DEBIAN_VERSION-backports main contrib non-free non-free-firmware
deb-src https://deb.debian.org/debian $DEBIAN_VERSION-backports main contrib non-free non-free-firmware	
APT
	DEBIAN_FRONTEND=noninteractive
    apt-get update -y 
    [ $? -eq 0   ] || { echo "Debian Update Fail, check your network or config!!!"; exit 1 ; }
		# Restart services during package upgrades without asking
		echo '* libraries/restart-without-asking boolean true' | debconf-set-selections
		# install the package maintainer's version
    apt -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confnew" full-upgrade -y
    
    apt -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confnew" install rng-tools5 sudo ssh openssl bash-completion ssh iptables-persistent vim openssl curl zip vim gnupg2 openssh-server -y
	apt clean 
}

debian_enable_BBR(){
	cat >> /etc/sysctl.d/98-network-custom.conf << BBR
net.core.default_qdisc=fq
net.ipv4.tcp_congestion_control=bbr
net.ipv4.ip_forward = 1
fs.file-max = 51200
BBR
echo 'net.ipv4.tcp_fastopen=3' > /etc/sysctl.d/98-tcp_fastopen.conf
sysctl --system
sysctl -p
sysctl net.ipv4.tcp_available_congestion_control
sysctl -w net.ipv4.tcp_fastopen=3
}

add_common_user_login(){
	useradd -m -s /usr/bin/bash $USER_TO_ADD
	su  $USER_TO_ADD << 'EOF'
	ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
	cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
	cat '#' >> ~/.ssh/authorized_keys
	chmod 0600 ~/.ssh/authorized_keys
	echo "use this id_rsa key file login:"
	cat ~/.ssh/id_rsa
	#add another public key
	echo '# It is a example. ssh-rsa AAAAB3NzaC1y...JgFvQ== hans@localhost' >> ~/.ssh/authorized_keys
    cp /etc/skel/.bashrc /etc/skel/.profile  ~/
EOF
}

debian_config_ssh(){
	rm -f /etc/ssh/ssh_host_*
	dpkg-reconfigure  openssh-server
	# apt-get purge openssh-server -y && apt-get install openssh-server -y
	# change default port, forbident root login and password login 
	if [[ "$(grep "Include /etc/ssh/sshd_config.d/" /etc/ssh/sshd_config)" ]]; then
		# support kali & debian11 and later
		echo "Port $SSH_PORT" > /etc/ssh/sshd_config.d/my.conf
		echo "PasswordAuthentication no" >> /etc/ssh/sshd_config.d/my.conf
		echo "PermitRootLogin no" >> /etc/ssh/sshd_config.d/my.conf
	else
		# debian10 and befor
		sed -i 's/^Port/#Port/g' /etc/ssh/sshd_config
		sed -i 's/^PasswordAuthentication/#PasswordAuthentication/g' /etc/ssh/sshd_config
		sed -i 's/^PermitRootLogin/#PermitRootLogin/g' /etc/ssh/sshd_config
		echo "Port $SSH_PORT" |  tee -a /etc/ssh/sshd_config
		echo "PasswordAuthentication no" |  tee -a /etc/ssh/sshd_config
		echo "PermitRootLogin no" |  tee -a /etc/ssh/sshd_config
	fi
	systemctl restart ssh
}

debian_config_iptables_firewall(){
	# set iptables to legacy,some kernel may not support nftables
	update-alternatives --set iptables /usr/sbin/iptables-legacy
	update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
	update-alternatives --set arptables /usr/sbin/arptables-legacy
	update-alternatives --set ebtables /usr/sbin/ebtables-legacy
	cat > ~/iptables.sh << HAN
#!/bin/bash
# IPV4
iptables -F
iptables -X
iptables -Z
iptables -t nat -F
iptables -t nat -X
iptables -t nat -Z
iptables -A INPUT -s 127.0.0.1 -d 127.0.0.1 -j ACCEPT
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
# IPV6
ip6tables -F
ip6tables -X
ip6tables -Z
ip6tables -t nat -F
ip6tables -t nat -X
ip6tables -t nat -Z
ip6tables -A INPUT -s ::1 -d ::1 -j ACCEPT
ip6tables -A INPUT -i lo -j ACCEPT
ip6tables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
# Allow DHCPv6 from LAN only
ip6tables -A INPUT -m state --state NEW -m udp -p udp -s fe80::/10 --dport 546 -j ACCEPT
#
iptables -A INPUT -p tcp --dport $SSH_PORT -j ACCEPT
ip6tables -A INPUT -p tcp --dport $SSH_PORT -j ACCEPT

# icmp
iptables -A INPUT -p icmp   -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT 
ip6tables -A INPUT -p ipv6-icmp -j ACCEPT

iptables -A INPUT -j DROP
iptables -A OUTPUT -j ACCEPT
iptables -A FORWARD -j ACCEPT

ip6tables -A OUTPUT -p icmp  -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
ip6tables -A INPUT -p icmp   -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT 
ip6tables -P INPUT DROP
ip6tables -P FORWARD  ACCEPT
ip6tables -P OUTPUT  ACCEPT

HAN
	chmod +x ~/iptables.sh
	~/iptables.sh
	netfilter-persistent save
	netfilter-persistent reload
	systemctl enable netfilter-persistent
}

debian_cleaning(){
	find /var/log/ -type f -regex '.*\.[0-9]+\.gz$' -delete
}

############## main() ##############
check_if_running_as_root

if [[ "$(type -p apt)" ]]; then
    debian_update
	[ $? -eq 0 ] || { echo "Debian Update Fail, check your network or config!!!"; exit 1 ; }
	
    debian_enable_BBR
	[ $? -eq 0 ] || { echo "enable BBR Fail!!!"; exit 1 ; }
	
	add_common_user_login
	[ $? -eq 0 ] || { echo "add user Fail!!!"; exit 1 ; }
	
	debian_config_ssh
	[ $? -eq 0 ] || { echo "SSH Config Fail!!!"; exit 1 ; }
	
	debian_config_iptables_firewall
	[ $? -eq 0 ] || { echo "Iptables Config Fail!!!"; exit 1 ; }
	
	debian_cleaning
	[ $? -eq 0 ] || { echo "Cleaning Operation Fail!!!"; exit 1 ; }
	
elif [[ "$(type -p dnf)" ]]; then
    
	exit 0
elif [[ "$(type -p yum)" ]]; then
	
    exit 0
elif [[ "$(type -p zypper)" ]]; then
    exit 0
elif [[ "$(type -p pacman)" ]]; then
    exit 0
else
    echo "error: This operating system is not supported."
    exit 1
fi

echo "$(tput setaf 1)SSH Port:$(tput sgr 0)"$SSH_PORT
echo "$(tput setaf 1)LoginUser:$(tput sgr 0)"$USER_TO_ADD
echo "$(tput setaf 1)Login KeyFile: /home/$USER_TO_ADD/.ssh/id_rsa$(tput sgr 0)"
