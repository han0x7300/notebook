#!/bin/bash
# find which process is sending icmp traffic
# run as root
# install tcpdump first.
# Written by han0x7300 <han0x7300@gmail.com>
# Last updated on, Mar-05-2021
touch /tmp/icmp.logs
echo "results will save in /tmp/icmp.logs"
while [ 1 -eq 1 ]
do
    results=$(ss -anpw | sed '1d')
    if [[ -z $results ]]
    then
            sleep 0.1
    else    
            tcpdump 'icmp' -c 1 -f -q  2>/dev/null >> /tmp/icmp.logs
            echo $results >> /tmp/icmp.logs
            date >> /tmp/icmp.logs 
            echo "" >> /tmp/icmp.logs
            sleep 0.5
    fi
done
