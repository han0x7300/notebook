#!/bin/bash

<<COMMENT1
Iptables port forward script.

This scripts can forward domain name and port.

update_rule 4444 jd.com 5555
This will listen port:4444 and forward to jd.com:5555

You can combine with crontab to regulare update iptables rules.

This scripts can delete old rules as well, when domain's ip changed.

Crontab example:
(crontab -l; echo "* * * * * /bin/bash /root/port_forward.sh") | sort -u | crontab -


First parameter: listen port
Second parameter: destination domain 
Third parameter: destination port 
COMMENT1

update_rule() {

    dest_ip=$(  dig $2 A $2 AAAA +short | tail -1 )

    #dest_ip=142.250.70.238
    #dest_ip=2607:f130:0:17d::3d30:3bd1
    if [[ $dest_ip =~ [0-9]+\.[0-9]+\.[0-9]+\.[0-9]+ ]]; then
        echo "It's IPV4!"
        ip_version=4
    elif [[ $dest_ip =~ [0-9a-z]+\: ]]; then
        echo "It's IPV6!"
        ip_version=6
    else 
        echo "error!"
        exit 1
    fi
    echo $dest_ip

    # delete exist rules
    # iptables -t nat -S | grep " --dport 2222 -j DNAT --to-destination"
    original_rules=$( iptables -t nat -S | grep " --dport $1 -j DNAT --to-destination" )
    #echo "original_rules:"
    #echo $original_rules
    IFS=$'\n'
    for i in $original_rules; 
    do
        echo "$i" | grep " --dport $1 -j DNAT --to-destination $dest_ip:$3"
        # if exist rules are not same with target rules, delete them.
        if [[ "$?" -eq 0 ]];then
            echo "find same ipv4 rule. skip"
            break
        else
            echo "delete rule:"
            j=$( echo $i | sed "s/-A /-D /g" )
            k="/usr/sbin/iptables -t nat $j "
            echo "$k"
            echo "$k" | bash 
        fi
    done 

    # ip6tables -t nat -S | grep " --dport 2222 -j DNAT --to-destination"
    original_rules=$( ip6tables -t nat -S | grep " --dport $1 -j DNAT --to-destination" )
    #echo "original_rules:"
    #echo $original_rules
    IFS=$'\n'
    for i in $original_rules; 
    do
        
        echo "$i" | grep " --dport $1 -j DNAT --to-destination \[$dest_ip\]:$3"
        # if exist rules are not same with target rules, delete them.
        if [[ "$?" -eq 0 ]];then
            echo "find same ipv6 rule. skip"
            break
        else
            echo "delete rule:"
            j=$( echo $i | sed "s/-A /-D /g" )
            k="/usr/sbin/ip6tables -t nat $j "
            echo "$k"
            echo "$k" | bash 
        fi
    done 

    # add new rules
    
    if [[ ip_version -eq 4 ]];then
        # iptables -t nat -A PREROUTING -p tcp --dport 2222 -j DNAT --to-destination 142.250.70.238:3333
        # iptables -t nat -A PREROUTING -p udp --dport 2222 -j DNAT --to-destination 142.250.70.238:3333
        /usr/sbin/iptables -t nat -A PREROUTING -p tcp --dport $1 -j DNAT --to-destination $dest_ip:$3
        /usr/sbin/iptables -t nat -A PREROUTING -p udp --dport $1 -j DNAT --to-destination $dest_ip:$3
    elif [[ ip_version -eq 6 ]]; then
        # ip6tables -t nat -A PREROUTING -p tcp --dport 2222 -j DNAT --to-destination [2607:f130:0:17d::3d30:3bd1]:3333
        # ip6tables -t nat -A PREROUTING -p udp --dport 2222 -j DNAT --to-destination [2607:f130:0:17d::3d30:3bd1]:3333
        /usr/sbin/ip6tables -t nat -A PREROUTING -p tcp --dport $1 -j DNAT --to-destination [$dest_ip]:$3
        /usr/sbin/ip6tables -t nat -A PREROUTING -p udp --dport $1 -j DNAT --to-destination [$dest_ip]:$3
    fi

    MASQUERADE_exist=$( /usr/sbin/iptables -t nat -S | grep " POSTROUTING -j MASQUERADE" )
    if [ -z "$MASQUERADE_exist" ]; then
        echo "MASQUERADE need to add."
        /usr/sbin/iptables -t nat -A POSTROUTING -j MASQUERADE

    fi

    MASQUERADE_exist=$( /usr/sbin/ip6tables -t nat -S | grep " POSTROUTING -j MASQUERADE" )
    if [ -z "$MASQUERADE_exist" ]; then
        echo "MASQUERADE need to add."
        /usr/sbin/ip6tables -t nat -A POSTROUTING -j MASQUERADE

    fi
    echo "$1 $2 $3 - Done."
}

dig

if [[ $? == 127 ]]; then 
    sudo apt -y update
    sudo apt -y install dnsutils
fi

# iptables -t nat -S | grep " --dport 2222 -j DNAT --to-destination"
# ip6tables -t nat -S | grep " --dport 2222 -j DNAT --to-destination"
update_rule 2222 bing.com 3333
update_rule 4444 jd.com 5555


