
# 导入工程
> [git 地址](https://github.com/rebeyond/memShell)

小改只要改 Agent.java 和 Attach.java 两个文件，改好改一下pom：
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>ParaShell</groupId>
  <artifactId>ParaShell</artifactId>
  <version>0.0.1-SNAPSHOT</version>

  <build>
    <sourceDirectory>src</sourceDirectory>
    <plugins>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.6.1</version>
        <configuration>
          <source>1.6</source>
          <target>1.6</target>
          <encoding>UTF-8</encoding>
        </configuration>
      </plugin>
    </plugins>
  </build>
  <dependencies>
  <!-- https://mvnrepository.com/artifact/org.javassist/javassist -->
 <dependency>
     <groupId>org.javassist</groupId>
     <artifactId>javassist</artifactId>
     <version>3.22.0-GA</version>
 </dependency>
 <dependency>
           <groupId>com.sun</groupId>
           <artifactId>tools</artifactId>
           <version>1.8</version>
           <scope>system</scope>
           <systemPath>${env.JAVA_HOME}/lib/tools.jar</systemPath>
 </dependency>
 <!-- https://mvnrepository.com/artifact/javax.servlet/javax.servlet-api -->
 <dependency>
     <groupId>javax.servlet</groupId>
     <artifactId>javax.servlet-api</artifactId>
     <version>4.0.1</version>
     <scope>provided</scope>
 </dependency>
  
    </dependencies>
</project>
```
改完命令行编译一下：
```
mvn clean package
```
# 替换
去target下，改了什么类，分别替换 [git release]( https://github.com/rebeyond/memShell/releases) 中的 inject.jar和agent.jar里相应的类，不能全替换。
按官方文档的运行就可以了。


















