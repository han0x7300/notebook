# 准备
前两个月学juiceshop的时候，理解不了juiceshop中的Oauth2问题，索性就自己实现一下，顺便学一下spring framework.

实现的过程中主要参考了如下文档：
* https://developers.google.com/identity/protocols/OpenIDConnect
* https://blog.csdn.net/manongxiaomei/article/details/67633655
* https://spring.io/blog/2011/11/30/cross-site-request-forgery-and-oauth2 
* https://developers.google.com/oauthplayground/  

# 实现
随便写写的，主要是理解Oauth2的流程。写的很粗糙。

在eclipse里创建动态web项目。

com.hans.HttpClient.java
````
package com.hans;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * sendPost方法用于发送post请求。
 * 
 * sendGet请求用于发送get请求。
 * 
 */
public class HttpClient {

	ObjectMapper objectMapper = new ObjectMapper();
	String AccessTokenResults = "";
	String UserProfileResult = "";
	
	/**
	 * 发送一个post请求，
	 * formparams是post的body，post请求body的格式是"abc=xxx&bbc=yyy"
	 * url是请求的地址
	 * header是头要放的内容
	 * 把结果Body的内容以字符串形式返回。
	 * */
	protected String sendPost(List<NameValuePair> formparams, String url, HashMap<String, String> header)
			throws Exception {
		// 接受所有格式的ssl
		SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true)
				.build();
		// 初始化构造
		CloseableHttpClient httpclient = HttpClients.custom().setSSLContext(sslContext)
				.setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
		// 加post要传的参数
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
		HttpPost httppost = new HttpPost(url);
		// 加post的header
		if (header != null) {
			for (Map.Entry<String, String> entry : header.entrySet()) {
				httppost.setHeader(entry.getKey(), entry.getValue());
			}
		}

		httppost.setEntity(entity);
		try {
			HttpResponse response = httpclient.execute(httppost);
			StatusLine statusLine = response.getStatusLine();
			HttpEntity entityRes = response.getEntity();

			if (statusLine.getStatusCode() >= 300) {
				throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
			}
			/*
			 * if (entity == null) { throw new
			 * ClientProtocolException("Response contains no content"); }
			 */
			ContentType contentType = ContentType.getOrDefault(entityRes);
			Charset charset = contentType.getCharset();
			Reader reader = new InputStreamReader(entityRes.getContent(), charset);
			// return gson.fromJson(reader, MyJsonObject.class);
			// System.out.println("123");
			int data = reader.read();
			AccessTokenResults = "";
			while (data != -1) {
				char theChar = (char) data;
				AccessTokenResults = AccessTokenResults + theChar;
				//System.out.print(theChar);
				data = reader.read();
			}
			//System.out.print(AccessTokenResults);
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("wrong");
		} finally {
			// response.close();
			httpclient.close();
		}

		return AccessTokenResults;
	}
	/**
	 * 发送一个Get请求，
	 * url是目标地址
	 * header是http头。
	 * */
	protected String sendGet(String url, HashMap<String, String> header) throws Exception {
		// 接受所有格式的ssl
		SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, (certificate, authType) -> true)
				.build();
		// 初始化构造
		CloseableHttpClient httpClient = HttpClients.custom().setSSLContext(sslContext)
				.setSSLHostnameVerifier(new NoopHostnameVerifier()).build();

		HttpGet httpGet = new HttpGet(url);
		// 便利Map ,加到http头里
		for (Map.Entry<String, String> entry : header.entrySet()) {
			httpGet.addHeader(entry.getKey(), entry.getValue());
		}

		try {
			HttpResponse response = httpClient.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			HttpEntity entityRes = response.getEntity();

			if (statusLine.getStatusCode() >= 300) {
				throw new HttpResponseException(statusLine.getStatusCode(), statusLine.getReasonPhrase());
			}
			ContentType contentType = ContentType.getOrDefault(entityRes);
			Charset charset = contentType.getCharset();
			Reader reader = new InputStreamReader(entityRes.getContent(), charset);
			int data = reader.read();
			UserProfileResult = "";
			while (data != -1) {
				char theChar = (char) data;
				UserProfileResult = UserProfileResult + theChar;
				// System.out.print(theChar);
				data = reader.read();
			}
			//System.out.print(UserProfileResult);
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpClient.close();
		}
		return UserProfileResult;
	}

}
````
com.hans.TokenResponse.java
````
package com.hans;

public class TokenResponse {
	private String code;
	private String state;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
}

````
com.hans.Oauth2Controller.java
````
package com.hans;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.Md5Crypt;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Controller;

@Controller
public class Oauth2Controller {

	private String CLIENT_ID = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com";
	private String CLIENT_SECRET = "xxxxxxxxxxxxxx";
	private String REDIRECT_URL = "http://hans.com:8080/Oauth2/GoogleUserInfo";
	private String GANT_TYPE = "authorization_code";
	// private String
	// GET_ACCESS_TOKEN_URL="https://www.googleapis.com/oauth2/v4/token";
	private String GET_ACCESS_TOKEN_URL = "https://www.googleapis.com/oauth2/v4/token";

	HttpClient httpClient = new HttpClient();
	ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(value = "/")
	public String mainPage() {
		System.out.println("登录到Login页面");
		return "login";
	}

	// 用户访问 http://localhost:8080/Oauth2/login后，页面上有一个谷歌登录按钮，等待点击
	@RequestMapping(value = "/login")
	public String toIndex() {
		System.out.println("登录到Login页面");
		return "login";
	}

	// "已获授权的重定向 URI" 指定了用户登录完谷歌账号跳转到这里。
	// 在https://console.developers.google.com/apis/credentials/oauthclient里设置
	@RequestMapping(value = "/sendRedirect")
	public RedirectView sendRedirect(@CookieValue("JSESSIONID") String jssessonId, RedirectAttributes attr) {
		// System.out.println("JSSESIONID is :------------------------------->" +
		// jssessonId);
		// 根据jssessionId的MD5加盐生成state,防止被猜到造成冒充accessToken的csrf攻击。
		String state = Md5Crypt.apr1Crypt(jssessonId, "hans0v0VeryHandSome").toUpperCase();
		System.out.println("state is :------------------------------->" + state);
		// 这里应该显示一个同意的屏幕，允许用户授哪些权限，这里就省略了，直接默认权限
		// System.out.println("同意并获取access_token");
		// state用来防止CSRF攻击
		// String state = UUID.randomUUID() + "";
		attr.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
		attr.addAttribute("client_id", "994970601902-rmigmdda603kk265j3d5u3fed5s5ht9e.apps.googleusercontent.com");
		attr.addAttribute("response_type", "code");
		attr.addAttribute("scope", "https://www.googleapis.com/auth/userinfo.profile");
		attr.addAttribute("redirect_uri", "http://hans.com:8080/Oauth2/GoogleUserInfo");
		attr.addAttribute("state", state);
		// attr.addAttribute("login_hint","");
		// nonce 用作减缓在Client和Authorization Servers被攻击的情况，
		// 请求加这个参数，Authorization Servers的响应就必须要原封不动的返回这个参数。
		// 先暂时用abc123这个替代。
		attr.addAttribute("nonce", "abc123");
		// attr.addAttribute("","");
		// 让浏览器去谷歌那里授权
		return new RedirectView("https://accounts.google.com/o/oauth2/v2/auth");
	}

	@RequestMapping(value = "/GoogleUserInfo")
	@ResponseBody
	public Object login(@ModelAttribute("tokenResponse") TokenResponse tokenResponse,
			@CookieValue("JSESSIONID") String jssessonId) throws Exception {
		String state = Md5Crypt.apr1Crypt(jssessonId, "hans0v0VeryHandSome").toUpperCase();
		System.out.println("state should be :------------------------------->" + state);
		// 根据jssesionId 校验state
		String userState = tokenResponse.getState();
		if (userState.equals(state)) {
			// state校验通过，才去获取用户信息
			String code = tokenResponse.getCode();
			String userProfile = "";
			// System.out.println(code);
			// 构造一个请求，用code获取AcceessToken
			List<NameValuePair> gettinbAccessTokenForm = new ArrayList<NameValuePair>();
			gettinbAccessTokenForm.add(new BasicNameValuePair("code", code));
			gettinbAccessTokenForm.add(new BasicNameValuePair("client_id", CLIENT_ID));
			gettinbAccessTokenForm.add(new BasicNameValuePair("client_secret", CLIENT_SECRET));
			gettinbAccessTokenForm.add(new BasicNameValuePair("redirect_uri", REDIRECT_URL));
			gettinbAccessTokenForm.add(new BasicNameValuePair("grant_type", GANT_TYPE));
			try {
				String AccessTokenResults = httpClient.sendPost(gettinbAccessTokenForm, GET_ACCESS_TOKEN_URL, null);
				// System.out.println(AccessTokenResults);
				// 从json里获取access_token
				JsonNode jsonNode = objectMapper.readTree(AccessTokenResults);
				String access_token = jsonNode.get("access_token").asText();
				// 用access_token构造 header，获取用户信息
				HashMap<String, String> hm = new HashMap<String, String>();
				hm.put("Host", "www.googleapis.com");
				hm.put("Content-length", "0");
				hm.put("Authorization", "Bearer " + access_token);
				userProfile = httpClient.sendGet("https://www.googleapis.com/oauth2/v2/userinfo", hm);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// System.out.println("获取idToken错误。");
			}
			if (userProfile.isEmpty()) {
				return "500";
			} else {
				return userProfile;
			}
		// state 校验不通过，就返回403错误
		} else {
			return "403";
		}
	}

}

````
WEB-INF/JSP/login.jsp
````
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="xxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
  </head>
  <body>
    <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
    <script>
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        	//假装去授权页面，其实要看哪些都是我后台设置的。
        	location.href = "http://hans.com:8080/Oauth2/sendRedirect"; 
        	<!--
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());
		-->
        // The ID token you need to pass to your backend:
        //var id_token = googleUser.getAuthResponse().id_token;
        //console.log("ID Token: " + id_token);
      };
    </script>
  </body>
</html>
````
WEB-INF/web.xml
````
<web-app id = "WebApp_ID" version = "2.4"
   xmlns = "http://java.sun.com/xml/ns/j2ee" 
   xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation = "http://java.sun.com/xml/ns/j2ee 
   http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">
	<display-name>Oatuth2.0</display-name>
   <servlet>
      <servlet-name>Oauth2</servlet-name>
      <servlet-class>
         org.springframework.web.servlet.DispatcherServlet
      </servlet-class>
      <load-on-startup>1</load-on-startup>
   </servlet>

   <servlet-mapping>
      <servlet-name>Oauth2</servlet-name>
      <url-pattern>/</url-pattern>
   </servlet-mapping>	
	   
</web-app>
````
WEB-INF/Oauth2-servlet.xml
````
<beans xmlns = "http://www.springframework.org/schema/beans"
   xmlns:context = "http://www.springframework.org/schema/context"
   xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
   xsi:schemaLocation = "http://www.springframework.org/schema/beans     
   http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
   http://www.springframework.org/schema/context 
   http://www.springframework.org/schema/context/spring-context-3.0.xsd">
	<context:component-scan base-package="com.hans"></context:component-scan>
	<bean class = "org.springframework.web.servlet.view.InternalResourceViewResolver">
		<property name="prefix" value = "/WEB-INF/JSP/"/>
		<property name="suffix" value = ".jsp" />
	</bean>
</beans>
````
# 理解
## state参数理解
Oauth2认证流程里有个参数，叫state。是谷歌强烈推荐配置的。并且还要client做校验，防止csrf攻击。
state这个参数要用某种攻击者猜不到的形式生成。
最好是基于jsessionid以某种算法生成。
这里假设攻击者在client也有合法账户。也能生成攻击者账户的access_token。
如果某个普通用户发来的accessToken是攻击者xss生成的，也就是用的是攻击者的accessToken，攻击者想强制更改登录的用户账户，变成攻击者的账户,
那这时client要以负责任的态度帮用户防护，防护就是做state校验。
正常的场景，client会给用户一个正确的state,所以不会有问题，如果是伪造的，clien基于sessionId的加密算法去做state参数校验不通过，
就是是伪造的请求，应当拒绝请求。


## juiceshop中Oauth相关问题理解

[配置juiceshop的clientId和screte.](https://github.com/bkimminich/juice-shop/blob/master/TROUBLESHOOTING.md)

juiceshop中关于Oauth的两个问题：
1.  Log in with Bjoern's user account without previously changing his password, applying SQL Injection, or hacking his Google account.
2. Exploit OAuth 2.0 to log in with the Chief Information Security Officer's user account

第一个Oauth问题没啥好说的，虽然juice-shop.min.js看不太懂，但是Js里还是很明显的泄露了某些账号的密码生成方式：
````
}]), angular.module("juiceShop").controller("OAuthController", ["$rootScope", "$window", "$location", "$cookies", "$base64", "UserService", function(n, t, a, o, r, i) {
    "use strict";
    function s(e) {
        i.login({
            email: e.email,
            password: r.encode(e.email),
````
也就是基于账号的base64码作为密码，不过我现在还搞不懂，本杰明的账号是怎么猜到的，尤其是@juice-sh.op这个后缀,还是靠之前SQL注入的经验？

第二个Oauth问题，其实只是个逻辑问题。如果：POST /rest/user/login  这个请求的POST参数里有"oauth":true这个值，且email和基于email的base64加密的密码 值匹配，那么header里的“X-User-Email:”指啥用户，就可以获取啥用户的email和合法token。用获取到的token放到后面的请求头的“Authorization: Bearer ”后面，就可以用其他用户登录。

相当于用这个请求，可以随便获取任何账户的token,然后把浏览器里的cookie里的token换一下，就可以以任何账户的身份登录了。这个请求直接导致Oauth2的校验没有任何意义。

由于accessToken根本就没发给client,也就是juice shop应用服务器，client也就无法校验你谷歌登录的请求，才会有上面的情况。具体情况用burp拦截一下就能看出来了。


